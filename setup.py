'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.

Description:
  File for compiling the Fortran code of the module. Run with
  
  python setup.py build_ext --inplace
'''

from numpy.distutils.core import Extension

ext = Extension(name='CLstunfti.ftools',sources =['CLstunfti/ftools.f95'])

if __name__ == "__main__":
  from numpy.distutils.core import setup
  setup(name        = 'ftools',
        version     ='1.00',
        description = "Fortran tools for CLstunfti",
        author      = "Axel Schild",
        ext_modules = [ext]
        )
