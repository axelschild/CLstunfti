'''
CLstunfti
by Axel Schild

This file is part of CLstunfti.

CLstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

CLstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.
  

Determine the EAL given an EMFP and IMFP.

  EAL  ... effective attenuation length
  EMFP ... mean free path for   elastic scattering
  IMFP ... mean free path for inelastic scattering
    
'''
import numpy                                            as np
from   scipy.optimize         import curve_fit
import CLstunfti.conversion_factors                     as conv
import CLstunfti.tools                                  as tools
from   CLstunfti.system       import class_system
from   CLstunfti.tools_eal    import compute_eal

# set a seed for reproducability of the test calculation
np.random.seed(1)

# *** USER INPUT SECTION *******************************************************
# input for CLstunfti should be in atomic units (and angles in radian), what is  
# given here is converted below...
F_plot       = True                  # flag for plotting
emfp_nm      = 0.3                   # EMFP in nm
imfp_factor  = 10                    # IMFP/EMFP
z0_max_nm    = -0.5                  # minimal considered starting position in nm (should be less than 0)
z0_min_nm    = -5.                   # maximal considered starting position in nm (should be less than 0)
z0_pts       = 6                     # number of starting depths for EAL computation
n_traj       = int(1e6)              # number of trajectories
datafile     = 'prop_data.h5'        # name of data file for propagation
theta_detect = 5*np.pi/180.          # detector opening angle, here 5 degrees; 
                                     # should be small, as the definition of the 
                                     # EAL depends on this parameter
                                     # set to None to count all trajectories
F_eff_r      = 0                     # use effective scattering length for 
                                     # elastic and inelastic scattering (should 
                                     # make not difference to the result)
# ******************************************************************************

# convert grid boundaries to atomic units
z0_max = z0_max_nm * conv.nm_to_au
z0_min = z0_min_nm * conv.nm_to_au

# load system data and set elastic & inelastic mean free paths
sdat      = class_system(infile=datafile)
sdat.emfp = emfp_nm * conv.nm_to_au
sdat.imfp = imfp_factor * sdat.emfp

# do the calculation
z0, counter, fitres, fiterr = compute_eal(z0_min,
                                          z0_max,
                                          z0_pts,
                                          n_traj,
                                          sdat,
                                          theta_detect,
                                          F_speak=True,
                                          F_eff_r=F_eff_r)

z0_nm      = z0 / conv.nm_to_au
eal_nm     = fitres[1] / conv.nm_to_au                # EAL
eal_nm_err = np.sqrt(fiterr[1,1]) / conv.nm_to_au     # standard deviation

# plot the results
if F_plot:
  z0_plot    = np.linspace(z0_min,z0_max,1000) # grid for plotting
  z0_nm_plot = z0_plot / conv.nm_to_au # grid for plotting
  import matplotlib.pyplot as plt
  plt.ion()
  plt.plot(z0_nm     ,counter                    ,'o',c='brown')
  plt.plot(z0_nm_plot,tools.eal(-z0_plot,*fitres),c='orange',
           label='av. num. of scatterings: %5.2f, EAL = %5.2f nm' % (imfp_factor,eal_nm))
  plt.title('EMFP = %5.2f nm' % (emfp_nm))
  plt.legend(loc=1)
  plt.xlabel('starting position in nm')
  plt.ylabel('normalized distribution of counted trajectories')
  plt.axis([z0_nm[0],z0_nm[-1],-0.05,1.05])
  plt.plot([z0_nm[0],z0_nm[-1]],[0,0],'k',lw=0.3)
  plt.plot([z0_nm[0],z0_nm[-1]],[1,1],'k',lw=0.3)
  plt.savefig('02_eal.pdf')
