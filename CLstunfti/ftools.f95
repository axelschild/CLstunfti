! CLstunfti
! by Axel Schild
! 
! This file is part of CLstunfti.
! 
! CLstunfti is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as 
! published by the Free Software Foundation, either version 3 of 
! the License, or any later version.
! 
! CLstunfti is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public 
! License along with CLstunfti.  If not, see <http://www.gnu.org/licenses/>.
! 
!
! Fortran helper functions for CLstunfti. Compile with
!   f2py -c --opt='-O3 -ffast-math' ftools.f95 -m ftools
! to use the subroutine in python. Check input order with
!   print(<subroutine_name>.__doc__)

! propagation routine, trajectories end at the surface
subroutine propagate(b_alive,   &  ! boolean array indicating alive trajectories (updated)
                     b_escaped, &  ! boolean array indicating escaped trajectories (updated)
                     r,         &  ! position vector (updated)
                     dr,        &  ! displacement (updated)
                     path_tot,  &  ! path travelled so far (updated)
                     path,      &  ! path travelled this time (updated)
                     ion_f,     &  ! integer indicating if we are right after ionization (ion_f = 1) or at any other step
                     i_alive,   &  ! index of alive trajectories
                     path_max,  &  ! maximum path to travel
                     t,         &  ! current polar angle (direction of this displacement) (converted to lab frame)
                     p,         &  ! current azimuthal angle (direction of this displacement) (converted to lab frame)
                     t_old,     &  ! previous polar angle (for rotation to lab frame)
                     p_old,     &  ! previous azimuthal angle (for rotation to lab frame)
                     ion_angle, &  ! angle giving the laser polarization axis
                     n,         &  ! number of trajectories (optional)
                     n_alive    )  ! number of alive trajectories (optional)
  ! ****************************************************************************
  ! Update position vector for change of direction defined by <t> and <p> 
  ! (w.r.t. current direction of motion), check if trajectories scatter 
  ! inelastic of if they escape.
  !
  ! NOTE: Update the number of alive trajectories after running!
  ! NOTE: After exit, the total travelled path is shorter than path_tot.
  ! ****************************************************************************
  implicit none
  logical(8), intent(inout) :: b_alive(n), b_escaped(n)
  real(8), intent(inout)    :: r(3,n), dr(3,n), path_tot(n), &
                               path(n), t_old(n), p_old(n)
  integer, intent(in)       :: ion_f, i_alive(n_alive), n, n_alive
  real(8), intent(in)       :: path_max(n), t(n), p(n), &
                               ion_angle
  real(8)                   :: s, c
  integer                   :: ii, jj
  s = sin(-ion_angle)
  c = cos(-ion_angle)
  do jj = 1, n_alive
    ii = i_alive(jj)+1
    ! compute displacements in relative frame
    dr(1,ii) = path(ii) * sin(t(ii)) * cos(p(ii))
    dr(2,ii) = path(ii) * sin(t(ii)) * sin(p(ii))
    dr(3,ii) = path(ii) * cos(t(ii))
    ! compute changes to the displacement vector
     if (ion_f .eq. 1) then
      ! rotate by laser polarization angle
      call apply_ion_angle( dr(:,ii), s, c )
    else
      ! compute displacements in lab frame, if called for scattering (not done for ionization)
      call rotate_back( dr(:,ii), t_old(ii), p_old(ii) )
    endif
    ! check if the trajectory ends outside of the liquid (z>0 is outside liquid)
    if ((r(3,ii)+dr(3,ii)) .gt. 0) then
      ! azimuthal and polar angle of the displacement in lab frame
      ! NOTE that t_old, p_old get updated here only for exited trajectories
      call cart2sph_ang(dr(:,ii),t_old(ii),p_old(ii))
      ! z-displacement to surface
      dr(3,ii) = -r(3,ii)
      ! path to surface (using azimuthal angle in lab frame)
      path(ii) = dr(3,ii) / cos(t_old(ii))
      ! x-, y-displacement to surface (using azimuthal and polar angle in lab frame)
      dr(1,ii) = path(ii) * sin(t_old(ii)) * cos(p_old(ii))
      dr(2,ii) = path(ii) * sin(t_old(ii)) * sin(p_old(ii))
      b_escaped(ii) = .true.
    endif
    ! update path
    path_tot(ii) = path_tot(ii) + path(ii)
    ! check if trajectory died
    if (path_tot(ii) .gt. path_max(ii)) then
      b_alive(ii)   = .false.
      b_escaped(ii) = .false.
    endif
    ! update coordinates
    r(:,ii) = r(:,ii) + dr(:,ii)
  enddo
end subroutine propagate

! propagation routine, trajectories end at the surface
subroutine propagate_tilted_surface( &
                     gamma,     &  ! tilting angle of the surface
                     b_alive,   &  ! boolean array indicating alive trajectories (updated)
                     b_escaped, &  ! boolean array indicating escaped trajectories (updated)
                     r,         &  ! position vector (updated)
                     dr,        &  ! displacement (updated)
                     path_tot,  &  ! path travelled so far (updated)
                     path,      &  ! path travelled this time (updated)
                     ion_f,     &  ! integer indicating if we are right after ionization (ion_f = 1) or at any other step
                     i_alive,   &  ! index of alive trajectories
                     path_max,  &  ! maximum path to travel
                     t,         &  ! current polar angle (direction of this displacement) (converted to lab frame)
                     p,         &  ! current azimuthal angle (direction of this displacement) (converted to lab frame)
                     t_old,     &  ! previous polar angle (for rotation to lab frame)
                     p_old,     &  ! previous azimuthal angle (for rotation to lab frame)
                     ion_angle, &  ! angle giving the laser polarization axis
                     n,         &  ! number of trajectories (optional)
                     n_alive    )  ! number of alive trajectories (optional)
  ! ****************************************************************************
  ! Update position vector for change of direction defined by <t> and <p> 
  ! (w.r.t. current direction of motion), check if trajectories scatter 
  ! inelastic of if they escape.
  !
  ! NOTE: Update the number of alive trajectories after running!
  ! NOTE: After exit, the total travelled path is shorter than path_tot.
  ! ****************************************************************************
  implicit none
  logical(8), intent(inout) :: b_alive(n), b_escaped(n)
  real(8), intent(inout)    :: r(3,n), dr(3,n), path_tot(n), &
                               path(n), t_old(n), p_old(n)
  integer, intent(in)       :: ion_f, i_alive(n_alive), n, n_alive
  real(8), intent(in)       :: path_max(n), t(n), p(n), &
                               ion_angle, gamma
  real(8)                   :: s, c, tg, a
  integer                   :: ii, jj
  s = sin(-ion_angle)
  c = cos(-ion_angle)
  tg = tan(gamma)
  do jj = 1, n_alive
    ii = i_alive(jj)+1
    ! compute displacements in relative frame
    dr(1,ii) = path(ii) * sin(t(ii)) * cos(p(ii))
    dr(2,ii) = path(ii) * sin(t(ii)) * sin(p(ii))
    dr(3,ii) = path(ii) * cos(t(ii))
    ! compute changes to the displacement vector
     if (ion_f .eq. 1) then
      ! rotate by laser polarization angle
      call apply_ion_angle( dr(:,ii), s, c )
    else
      ! compute displacements in lab frame, if called for scattering (not done for ionization)
      call rotate_back( dr(:,ii), t_old(ii), p_old(ii) )
    endif
    ! check if the trajectory ends outside of the liquid
    a = (r(2,ii)*tg-r(3,ii)) / (dr(3,ii)-dr(2,ii)*tg)
    if ( (a .gt. 0) .and. (a .lt. 1) ) then
      dr(:,ii) = a*dr(:,ii)
      b_escaped(ii) = .true.
      path(ii) = dr(3,ii) / cos(t(ii))
    endif
    ! update path
    path_tot(ii) = path_tot(ii) + path(ii)
    ! check if trajectory died
    if (path_tot(ii) .gt. path_max(ii)) then
      b_alive(ii)   = .false.
      b_escaped(ii) = .false.
    endif
    ! update coordinates
    r(:,ii) = r(:,ii) + dr(:,ii)
  enddo
end subroutine propagate_tilted_surface

! propagation routine, trajectories end at the surface with effective scattering 
! distance for elastic and inelastic scattering
subroutine propagate_r_eff(     &
                     b_alive,   &  ! boolean array indicating alive trajectories (updated)
                     b_escaped, &  ! boolean array indicating escaped trajectories (updated)
                     r,         &  ! position vector (updated)
                     dr,        &  ! displacement (updated)
                     path_tot,  &  ! path travelled so far (updated)
                     path,      &  ! path travelled this time (updated)
                     ifrac,     &  ! fraction of inelastic scattering
                     dice,      &  ! random numbers to test for inelastic scattering
                     ion_f,     &  ! integer indicating if we are right after ionization (ion_f = 1) or at any other step
                     i_alive,   &  ! index of alive trajectories
                     t,         &  ! current polar angle (direction of this displacement) (converted to lab frame)
                     p,         &  ! current azimuthal angle (direction of this displacement) (converted to lab frame)
                     t_old,     &  ! previous polar angle (for rotation to lab frame)
                     p_old,     &  ! previous azimuthal angle (for rotation to lab frame)
                     ion_angle, &  ! angle giving the laser polarization axis
                     n,         &  ! number of trajectories (optional)
                     n_alive    )  ! number of alive trajectories (optional)
  ! ****************************************************************************
  ! Update position vector for change of direction defined by <t> and <p> 
  ! (w.r.t. current direction of motion), check if trajectories scatter 
  ! inelastic of if they escape.
  !
  ! NOTE: Update the number of alive trajectories after running!
  ! NOTE: After exit, the total travelled path is shorter than path_tot.
  ! ****************************************************************************
  implicit none
  logical(8), intent(inout) :: b_alive(n), b_escaped(n)
  real(8), intent(inout)    :: r(3,n), dr(3,n), path_tot(n), &
                               path(n), t_old(n), p_old(n)
  integer, intent(in)       :: ion_f, i_alive(n_alive), n, n_alive
  real(8), intent(in)       :: t(n), p(n), ion_angle, ifrac, dice(n_alive)
  real(8)                   :: s, c
  integer                   :: ii, jj
  s = sin(-ion_angle)
  c = cos(-ion_angle)
  ! even if we know already now if inelastic scattering happens, we need to 
  ! test if the trajectory left the liquid before scattering
  do jj = 1, n_alive
    ii = i_alive(jj)+1
    ! compute displacements in relative frame
    dr(1,ii) = path(ii) * sin(t(ii)) * cos(p(ii))
    dr(2,ii) = path(ii) * sin(t(ii)) * sin(p(ii))
    dr(3,ii) = path(ii) * cos(t(ii))
    ! compute changes to the displacement vector
     if (ion_f .eq. 1) then
      ! rotate by laser polarization angle
      call apply_ion_angle( dr(:,ii), s, c )
    else
      ! compute displacements in lab frame, if called for scattering (not done for ionization)
      call rotate_back( dr(:,ii), t_old(ii), p_old(ii) )
    endif
    ! check if the trajectory ends outside of the liquid (z>0 is outside liquid)
    if ((r(3,ii)+dr(3,ii)) .gt. 0) then
      ! azimuthal and polar angle of the displacement in lab frame
      ! NOTE that t_old, p_old get updated here only for exited trajectories
      call cart2sph_ang(dr(:,ii),t_old(ii),p_old(ii))
      ! z-displacement to surface
      dr(3,ii) = -r(3,ii)
      ! path to surface (using azimuthal angle in lab frame)
      path(ii) = dr(3,ii) / cos(t_old(ii))
      ! x-, y-displacement to surface (using azimuthal and polar angle in lab frame)
      dr(1,ii) = path(ii) * sin(t_old(ii)) * cos(p_old(ii))
      dr(2,ii) = path(ii) * sin(t_old(ii)) * sin(p_old(ii))
      b_escaped(ii) = .true.
    endif
    ! update path
    path_tot(ii) = path_tot(ii) + path(ii)
    ! check if trajectory died
    if (dice(jj) .gt. ifrac) then
      b_alive(ii)   = .false.
      b_escaped(ii) = .false.
    endif
    ! update coordinates
    r(:,ii) = r(:,ii) + dr(:,ii)
  enddo
end subroutine propagate_r_eff

! propagation routine without inelastic scattering, trajectories end at the surface
subroutine propagate_noinel(&
                     b_escaped, &  ! boolean array indicating escaped trajectories (updated)
                     r,         &  ! position vector (updated)
                     dr,        &  ! displacement (updated)
                     path_tot,  &  ! path travelled so far (updated)
                     path,      &  ! path travelled this time (updated)
                     ion_f,     &  ! integer indicating if we are right after ionization (ion_f = 1) or at any other step
                     i_alive,   &  ! index of alive trajectories
                     t,         &  ! current polar angle (direction of this displacement) (converted to lab frame)
                     p,         &  ! current azimuthal angle (direction of this displacement) (converted to lab frame)
                     t_old,     &  ! previous polar angle (for rotation to lab frame)
                     p_old,     &  ! previous azimuthal angle (for rotation to lab frame)
                     ion_angle, &  ! angle giving the laser polarization axis
                     n,         &  ! number of trajectories (optional)
                     n_alive    )  ! number of alive trajectories (optional)
  ! ****************************************************************************
  ! Update position vector for change of direction defined by <t> and <p> 
  ! (w.r.t. current direction of motion), check if trajectories scatter 
  ! inelastic of if they escape.
  !
  ! NOTE: Update the number of alive trajectories after running!
  ! ****************************************************************************
  implicit none
  logical(8), intent(inout) :: b_escaped(n)
  real(8), intent(inout)    :: r(3,n), dr(3,n), path_tot(n), &
                               path(n), t_old(n), p_old(n)
  integer, intent(in)       :: ion_f, i_alive(n_alive), n, n_alive
  real(8), intent(in)       :: t(n), p(n), ion_angle
  real(8)                   :: s, c
  integer                   :: ii, jj
  s = sin(-ion_angle)
  c = cos(-ion_angle)
  do jj = 1, n_alive
    ii = i_alive(jj)+1
    ! compute displacements in relative frame
    dr(1,ii) = path(ii) * sin(t(ii)) * cos(p(ii))
    dr(2,ii) = path(ii) * sin(t(ii)) * sin(p(ii))
    dr(3,ii) = path(ii) * cos(t(ii))
    ! compute changes to the displacement vector
     if (ion_f .eq. 1) then
      ! rotate by laser polarization angle
      call apply_ion_angle( dr(:,ii), s, c )
    else
      ! compute displacements in lab frame, if called for scattering (not done for ionization)
      call rotate_back( dr(:,ii), t_old(ii), p_old(ii) )
    endif
    ! check if the trajectory ends outside of the liquid (z>0 is outside liquid)
    if ((r(3,ii)+dr(3,ii)) .gt. 0) then
      ! azimuthal and polar angle of the displacement in lab frame
      ! NOTE that t_old, p_old get updated here only for exited trajectories
      call cart2sph_ang( dr(:,ii), t_old(ii), p_old(ii) )
      ! z-displacement to surface
      dr(3,ii) = -r(3,ii)
      ! path to surface (using azimuthal angle in lab frame)
      path(ii) = dr(3,ii) / cos(t_old(ii))
      ! x-, y-displacement to surface (using azimuthal and polar angle in lab frame)
      dr(1,ii) = path(ii) * sin(t_old(ii)) * cos(p_old(ii))
      dr(2,ii) = path(ii) * sin(t_old(ii)) * sin(p_old(ii))
      b_escaped(ii) = .true.
    endif
    ! update path
    path_tot(ii) = path_tot(ii) + path(ii)
    ! update coordinates
    r(:,ii) = r(:,ii) + dr(:,ii)
  enddo
end subroutine propagate_noinel

! propagation routine, no surface
subroutine propagate_nosurf & 
                    (r,         &  ! position vector (updated)
                     dr,        &  ! displacement (updated)
                     path,      &  ! fixed path to travel
                     i_alive,   &  ! index of alive trajectories
                     t,         &  ! current polar angle, lab frame
                     p,         &  ! current azimuthal angle, lab frame
                     n,         &  ! number of trajectories (optional)
                     n_alive    )  ! number of alive trajectories (optional)
  ! ****************************************************************************
  ! Update position vector for change of direction defined by <t> and <p> 
  ! (defined in the lab frame)
  ! ****************************************************************************
  implicit none
  real(8), intent(inout)    :: r(3,n), dr(3,n), path(n)
  integer, intent(in)       :: i_alive(n_alive), n, n_alive
  real(8), intent(in)       :: t(n), p(n)
  integer                   :: ii, jj
  do jj = 1, n_alive
    ii = i_alive(jj)+1
    ! compute displacements in lab frame
    dr(1,ii) = path(ii) * sin(t(ii)) * cos(p(ii))
    dr(2,ii) = path(ii) * sin(t(ii)) * sin(p(ii))
    dr(3,ii) = path(ii) * cos(t(ii))
    ! update coordinates
    r(:,ii) = r(:,ii) + dr(:,ii)
  enddo
end subroutine propagate_nosurf

! propagation routine for fixed distance, no surface, no inelastic scattering
subroutine propagate_fixed(     & 
                     r,         &  ! position vector (updated)
                     dr,        &  ! displacement (updated)
                     path,      &  ! path to travel
                     ion_f,     &  ! integer indicating if we are right after ionization (ion_f = 1) or at any other step
                     i_alive,   &  ! index of alive trajectories
                     t,         &  ! current polar angle (direction of this displacement) (converted to lab frame)
                     p,         &  ! current azimuthal angle (direction of this displacement) (converted to lab frame)
                     t_old,     &  ! previous polar angle (for rotation to lab frame)
                     p_old,     &  ! previous azimuthal angle (for rotation to lab frame)
                     ion_angle, &  ! angle giving the laser polarization axis
                     n,         &  ! number of trajectories (optional)
                     n_alive    )  ! number of alive trajectories (optional)
  ! ****************************************************************************
  ! Update position vector for change of direction defined by <t> and <p> 
  ! (w.r.t. current direction of motion), check if trajectories scatter 
  ! inelastic of if they escape.
  ! ****************************************************************************
  implicit none
  real(8), intent(inout)    :: r(3,n), dr(3,n), path, t_old(n), p_old(n)
  integer, intent(in)       :: ion_f, i_alive(n_alive), n, n_alive
  real(8), intent(in)       :: t(n), p(n), ion_angle
  real(8)                   :: s, c
  integer                   :: ii, jj
  s = sin(-ion_angle)
  c = cos(-ion_angle)
  do jj = 1, n_alive
    ii = i_alive(jj)+1
    ! compute displacements in relative frame
    dr(1,ii) = path * sin(t(ii)) * cos(p(ii))
    dr(2,ii) = path * sin(t(ii)) * sin(p(ii))
    dr(3,ii) = path * cos(t(ii))
    ! compute changes to the displacement vector
     if (ion_f .eq. 1) then
      ! rotate by laser polarization angle
      call apply_ion_angle( dr(:,ii), s, c )
    else
      ! compute displacements in lab frame, if called for scattering (not done for ionization)
      call rotate_back( dr(:,ii), t_old(ii), p_old(ii) )
    endif
    ! update coordinates
    r(:,ii) = r(:,ii) + dr(:,ii)
  enddo
end subroutine propagate_fixed

subroutine rotate_back(dr,t_old,p_old)
  ! ****************************************************************************
  ! Given the polar angle <t_old> and azimuthal angle <p_old> defining the 
  ! previous direction of motion, rotate the displacement vector <dr> back to 
  ! the lab frame.
  ! ****************************************************************************
  implicit none
  real(8), intent(inout) :: dr(3)
  real(8), intent(in)    :: t_old, p_old
  real(8)                :: x_new, y_new, z_new, ct, st, cp, sp
  ct = cos(-t_old)
  st = sin(-t_old)
  cp = cos(p_old)
  sp = sin(p_old)
  
  ! Rodrigues' rotation formula for rotating the vector dr around the axis 
  ! perpendicular to the direction given by (t_old, p_old) and e_z by the 
  ! angle t_old measured from (t_old, p_old) towards e_z
  x_new = ct*dr(1) + (ct-1)*sp*(cp*dr(2)-sp*dr(1)) - st*cp*dr(3)
  y_new = ct*dr(2) + (ct-1)*cp*(sp*dr(1)-cp*dr(2)) - st*sp*dr(3)
  z_new = ct*dr(3) +  st      *(cp*dr(1)+sp*dr(2))
  
  dr(1) = x_new
  dr(2) = y_new
  dr(3) = z_new
  
end subroutine rotate_back

subroutine cart2sph_ang(r,t,p)
  ! ****************************************************************************
  ! Compute azimuthal angle <t> and polar angle <p> for input vector <r> in 
  ! Cartesian coordinates (physicist's convention, i.e. according to ISO)
  !
  ! WARNING: Fortran atan2 behaves strange, hence the unexpected line for <t>
  !          instead of "t = atan2(r(3),sqrt(r(1)**2+r(2)**2))".
  !          There should be a file "test_cart2sph.py" which can be used to 
  !          check if this routine is correct with the version of Fortran that
  !          shall be used.
  ! ****************************************************************************
  implicit none
  real(8), intent(out) :: t, p
  real(8), intent(in)  :: r(3)
  real(8), parameter   :: c_pi  = 3.1415926535897932d0
  real(8), parameter   :: c_pi2 = 1.5707963267948966d0
  p = atan2( r(2),r(1) )
  t = atan2(-r(3),sqrt(r(1)**2+r(2)**2)) + c_pi2
end subroutine

subroutine apply_ion_angle(dr, s, c)
  ! ****************************************************************************
  ! Rotate dr by laser polarization angle t. It is assumed that s = sin(-t) and
  ! c = cos(-t), because this needs not to be calculates for every trajectory
  ! again. Note that this subroutine essentially applies the rotation matrix
  ! around the y-axis.
  ! ****************************************************************************
  implicit none
  real(8), intent(inout) :: dr(3)
  real(8), intent(in)    :: s, c
  real(8)                :: x_new, z_new
  x_new =  c*dr(1) + s*dr(3)
  z_new = -s*dr(1) + c*dr(3)
  dr(1) = x_new
  dr(3) = z_new
end subroutine apply_ion_angle

subroutine detect(d_count,    &  ! number of particles counted by detector (updated)
                  b_alive,    &  ! boolean array indicating alive trajectories (updated)
                  i_escaped,  &  ! index of all escaped trajectories
                  weights,    &  ! summation weights of the trajectories
                  t,          &  ! current polar angle
                  d_angle,    &  ! angle for which the detector detects
                  n,          &  ! number of trajectories
                  n_escaped,  &  ! number of escaped trajectories
                  d_size      &  ! number of detectors
                  )
  implicit none
  logical(8), intent(inout) :: b_alive(n)
  real(8), intent(inout)    :: d_count(d_size)
  real(8), intent(in)       :: t(n), d_angle(d_size), weights(n)
  integer, intent(in)       :: i_escaped(n_escaped), n, n_escaped, &
                               d_size
!   complex(8)                :: psi
  complex(8), parameter     :: c_i = (0.d0,1.d0)
  integer                   :: ii, jj, kk
  ! ****************************************************************************
  ! Compute wavefunction and detect.
  ! ****************************************************************************
  do jj = 1, n_escaped
    ii = i_escaped(jj)+1
    do kk = 1, d_size
      if (t(ii) < d_angle(kk)) then
        d_count(kk) = d_count(kk) + weights(ii)
      endif
    enddo
    b_alive(ii) = .false.
  enddo
end subroutine detect
